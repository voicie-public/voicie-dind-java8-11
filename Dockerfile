FROM gitlab/dind

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:openjdk-r/ppa && \
    apt-get -y update && \
    apt-get install -y openjdk-8-jdk && \
    apt-get install -y openjdk-11-jdk && \
    apt-get install -y sshpass
